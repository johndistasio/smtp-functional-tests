"""
Functional test suite for Scala SMTP server.

@author John DiStasio <jndistasio@gmail.com>
"""
import smtplib
import email.utils
from email.mime.text import MIMEText
import argparse

#Default destination server, port
email_server = '127.0.0.1'
email_port = 4444

#Default connection debug level
email_debug = True

#Default email details
email_to = 'test.recipient@localhost'
email_from = 'test.sender@localhost'

def send_email(serv, port, debug, to, frm):
    """Sends a single test message to the specified email server."""

    #Build out email message
    msg = MIMEText('Test Message Body')
    msg['To'] = email.utils.formataddr(('Test Recipient', to))
    msg['From'] = email.utils.formataddr(('Test Sender', frm))
    msg['Subject'] = 'Test Message Subject'

    destination_server = None

    try:
        destination_server = smtplib.SMTP(serv, port)
    except Exception as e:
        print e

    if destination_server:
	destination_server.set_debuglevel(debug)
        try:
            server.sendmail(frm, [to], msg)
        except SMTPRecipientsRefused:
            print 'Server didn\'t accept the destination address.'
        except SMTPHeloError:
            print 'Server didn\'t properly reply to the HELO greeting.'
        except SMTPSenderRefused:
            print 'Server didn\'t accept the sender address.'
        except SMTPDataError:
            print 'Server replied with an unexpected error code.'
        except:
	    print 'Unknown error.'
        finally:
            destination_server.quit()

if __name__ == '__main__':
  
    #Parse command line arguments
    parser = argparse.ArgumentParser(description = 'Functional test suite for an SMTP Server.') 
    parser.add_argument('-s', '--server', default = email_server,
		    help = 'The IPv4 address of the destination server. Defaults to localhost (127.0.0.1).')
    parser.add_argument('-p', '--port', default = email_port, type = int, 
		    help = 'The port on the destination server that is listening for connections. Defaults to 4444 for arbitrary reasons.')
    parser.add_argument('-t', '--to', default = email_to,
		    help = 'The desired recipient of the message. Defaults to \'test.recipient@localhost\'.')
    parser.add_argument('-f', '--from', default = email_from,
		    help = 'The sender of the message. Defaults to \'test.sender@localhost\'.')
    parser.add_argument('-v', '--verbose', default = email_debug, type = bool,
		    help = 'Show debug output. Defaults to True.')
    args = vars(parser.parse_args())

    if args['verbose']:
        print
        print 'Host: %(server)s:%(port)d' % args
        print '  To: %(to)s' % args
        print 'From: %(from)s' % args
        print

    send_email(args['server'], args['port'], args['verbose'], args['to'], args['from'])
